function MissingLetter(alphabet) {
    for (let i = 0; i < alphabet.length - 1; i++) {
        if (alphabet.charCodeAt(i + 1) - alphabet.charCodeAt(i) != 1) {
            return String.fromCharCode(alphabet.charCodeAt(i) + 1);
        }
    }
}

console.log(MissingLetter('oqrs'));